# Deploying an S3 Bucket with the AWS CDK
The purpose of this repository is to learn how to deploy an S3 bucket via the AWS CDK by using the AWS documentation. It is also an opportunity to leverage AWS Code Whisperer. 

## Usage
This repository is for educational purposes, meant to encourage the learning and usage of the AWS CDK. I will also provide instructions for how to set this up, and these instructions are mostly the same as what is found in this documentaton on deploying a first CDK [app](https://docs.aws.amazon.com/cdk/v2/guide/hello_world.html).

## Prerequisites
For those not using the AWS Cloud 9 environment, the required setup will be installing the following:

* Set up a virtual environment (language dependent)
* AWS cli (the most recent version)
* AWS CDK (the most recent version)
* Add the permissions to the IAM User/Role for the cloud instance or developer environment in Code Catalyst. Here is an image of the ones I used. I personally opted for adding permissions to the role, instead of making a new user.

![Permissions](images/permissions.png)

Those are the prerequisites. I took the typescript approach for this project, which is why I don't have a requirements.txt.
For those using AWS Cloud 9, verify the cli and cdk versions and continue onwards. 

## Setup Instructions

1. Make a directory for the project and navigate inside it.
```
$ mkdir hello-cdk
$ cd hello-cdk
```
2. Initialize the project. The language is optional : TypeScript, JavaScript, Python, Java, C#, Go
```
cdk init app --language typescript
```
3. Build the app (only applicable to TypeScript, Java, C#, and Go). This is favorable for catching type and syntax errors.
```
# the options
npm run build # TypeScript
mvn compile -q # Java
dotnet build src # C#
go build # Go
```
4. List your stack to verify that the name of your project is visible, indicating that it created correctly.
```
cdk ls
```
5. Add an S3 bucket under the code (likely in a lib folder) for your specific language, well provided by the script in this [tutorial by aws](https://docs.aws.amazon.com/cdk/v2/guide/hello_world.html). There are also other options to achieve this.
* Using Codewhisperer to make the construct for your desired AWS Bucket.
* Using templates to write the buckets.
I made modifications to allow for encryption and autodeletion, and the latter is good for permanently deleting objects if one needed to delete the whole stack later on to avoid costs.
As for encryption, there are many options, so choose what works best for the specific use case. 
```
# Modified Example for Typescript
import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { aws_s3 as s3 } from 'aws-cdk-lib';
import { aws_iam as iam } from 'aws-cdk-lib';

export class HelloCdkStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

  // make an S3 bucket with versioning and encryption
  new s3.Bucket(this, 'Project3Bucket',{
    versioned:true,
    encryption: s3.BucketEncryption.S3_MANAGED,
    removalPolicy: cdk.RemovalPolicy.DESTROY,
    autoDeleteObjects: true
    });
  }
}
```
6. Synthesize the app
This command creates a CloudFormation template for each stack in the app, which is why it needs to be a permission in your role. A cdk.out folder will be generated in your project directory, with a json showing the template.
```
cdk synth
```
7. Deploy the app
The app is deployed, and it will be visible on CloudFormation. The bucket is accesible through AWS CloudFormation after opening the stack name of your project. 
```
cdk deploy
```

## Result

Here is an example of my deployed app.
![Bucket](images/buckets_made.png)

## Licenses
Creative Commons.

## Status
This project is complete as of February 16th, 2024.